public class LongStack {
    private long number;
    private LongStack nextElement = null;

    public static void main(String[] argum) throws CloneNotSupportedException {
        System.out.println("=== STACK TESTS ===");
        LongStack stack = new LongStack();
        stack.push(10);
        stack.push(4);
        stack.push(3);
        stack.push(2);
        LongStack clonedStack = (LongStack) stack.clone();

        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        stack.push(5);
        System.out.println(stack.pop());
        System.out.println(stack.pop());

        try {
            System.out.println(stack.pop());
        } catch (RuntimeException e) {
            System.out.println("Runtime Exception caught.");
        }

        System.out.println(clonedStack.pop());
        System.out.println(clonedStack.pop());
        System.out.println(clonedStack.pop());
        System.out.println(clonedStack.pop());

        try {
            System.out.println(clonedStack.pop());
        } catch (RuntimeException e) {
            System.out.println("Runtime Exception caught.");
        }

        System.out.println("=== RPL TESTS ===");
        System.out.println(LongStack.interpret("5 6 +"));
        System.out.println(LongStack.interpret("10 5 -"));
        System.out.println(LongStack.interpret("-4 6 *"));
        System.out.println(LongStack.interpret("3333 3 /"));

        System.out.println("=== STACK OP TESTS ===");
        LongStack s1 = new LongStack();
        s1.push(5);
        s1.push(6);
        s1.op("+");
        System.out.println(s1.pop());
        LongStack s2 = new LongStack();
        s2.push(10);
        s2.push(5);
        s2.op("-");
        System.out.println(s2.pop());
        LongStack s3 = new LongStack();
        s3.push(-4);
        s3.push(6);
        s3.op("*");
        System.out.println(s3.pop());
        LongStack s4 = new LongStack();
        s4.push(3333);
        s4.push(3);
        s4.op("/");
        System.out.println(interpret("-234 -"));
    }

    LongStack() {
    }

    private LongStack(long a) {
        number = a;
    }

    private LongStack findLast() {
        LongStack lastElement = this;

        while (lastElement.nextElement != null) {
            lastElement = lastElement.nextElement;
        }

        return lastElement;
    }

    private LongStack findBeforeLast() {
        LongStack beforeLastElement = this;

        while (beforeLastElement.nextElement != null && beforeLastElement.nextElement.nextElement != null) {
            beforeLastElement = beforeLastElement.nextElement;
        }

        return beforeLastElement;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        LongStack clonedStack = new LongStack();
        LongStack currentLastElement = this;

        while (currentLastElement.nextElement != null) {
            currentLastElement = currentLastElement.nextElement;
            clonedStack.push(currentLastElement.number);
        }

        return clonedStack;
    }

    public boolean stEmpty() {
        return nextElement == null;
    }

    public void push(long a) {
        findLast().nextElement = new LongStack(a);
    }

    public long pop() {
        LongStack lastElement = findBeforeLast();

        if (lastElement.nextElement == null) {
            throw new RuntimeException("Stack underflow.");
        }

        long num = lastElement.nextElement.number;
        lastElement.nextElement = null;
        return num;
    }

    public void op(String s) throws RuntimeException {
        if (!s.equals("+") && !s.equals("-") && !s.equals("*") && !s.equals("/")) {
            throw new RuntimeException("Incorrect operation.");
        }

        if (nextElement == null || nextElement.nextElement == null) {
            throw new RuntimeException("Stack underflow.");
        }

        long b = pop();
        long a = pop();

        switch (s) {
            case "+":
                push(a + b);
                break;
            case "-":
                push(a - b);
                break;
            case "*":
                push(a * b);
                break;
            case "/":
                push(a / b);
                break;
        }
    }

    public long tos() {
        LongStack last = findLast();

        if (last == this) {
            throw new RuntimeException("Stack underflow");
        }

        return last.number;
    }

    @Override
    public boolean equals(Object o) {
        LongStack thisLastElement = this;
        LongStack cmpLastElement = (LongStack) o;

        if (thisLastElement.stEmpty() != cmpLastElement.stEmpty()) {
            return false;
        }

        while (thisLastElement.nextElement != null) {
            if (cmpLastElement.nextElement == null) {
                return false;
            }

            thisLastElement = thisLastElement.nextElement;
            cmpLastElement = cmpLastElement.nextElement;

            if (thisLastElement.number != cmpLastElement.number) {
                return false;
            }
        }

        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        LongStack lastElement = this;

        while (lastElement.nextElement != null) {
            lastElement = lastElement.nextElement;

            if (nextElement != lastElement) {
                sb.append(' ');
            }

            sb.append(lastElement.number);
        }

        return sb.toString();
    }

    public static long interpret(String pol) {
        LongStack stack = new LongStack();
        String[] parts = pol.trim().split("\\s+");

        for (String part : parts) {
            if (part.equals("+") || part.equals("-") || part.equals("*") || part.equals("/")) {
                try {
                    stack.op(part);
                } catch (RuntimeException e) {
                    throw new RuntimeException(e.getMessage() + " Pol: `" + pol + "`.");
                }

                continue;
            }

            try {
                stack.push(Long.parseLong(part));
            } catch (NumberFormatException e) {
                throw new RuntimeException("Incorrect format: unrecognized part `" + part + "` in `" + pol + "`.");
            }
        }

        long lastElem = stack.pop();

        if (!stack.stEmpty()) {
            throw new RuntimeException("Stack was left with redundant part(s). Pol: `" + pol + "`.");
        }

        return lastElem;
    }

}

